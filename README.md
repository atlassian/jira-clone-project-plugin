JIRA Clone Project
============

The JIRA Clone Project was created to make it easier for JIRA administrators to set up projects. You can now copy other project's settings (schemes, versions, components, etc.). 

You can download the plugin from [Marketplace](https://marketplace.atlassian.com/plugins/com.atlassian.jira.plugins.jira-clone-project-plugin) or you can build it from the source. 

Compatibility across JIRA versions
----------------------------------
`master` is compatible with 6.4.x
