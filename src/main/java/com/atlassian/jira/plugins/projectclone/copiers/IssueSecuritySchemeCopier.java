/*
 * Copyright © 2013 - 2015 Atlassian Pty Ltd. Licensed under the Apache License, Version
 *  2.0 (the "License"); you may not use this file except in compliance with the License. You
 *  may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless
 *  required by applicable law or agreed to in writing, software distributed under the License is
 *  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 *  either express or implied. See the License for the specific language governing permissions
 *  and limitations under the License.
 */

package com.atlassian.jira.plugins.projectclone.copiers;

import com.atlassian.jira.issue.security.IssueSecuritySchemeManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.ErrorCollection;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.0
 */
public class IssueSecuritySchemeCopier extends AbstractSchemeCopier {
	@Autowired
	protected IssueSecuritySchemeManager issueSecuritySchemeManager;

	@Override
	public void copySettings(Project project, Project newProject, ErrorCollection errorCollection) {
		copyScheme(issueSecuritySchemeManager, project, newProject);
	}
}
